import './App.css';
import books from './books.json'

function App() {
  return (
    <div className="App">
      {
        books && books.map( book => {
          return(
            <div key={book.id} className="Book-Container">
              <h3> {book.title} </h3>
            </div>
          )
        })
      }
    </div>
  );
}

export default App;
